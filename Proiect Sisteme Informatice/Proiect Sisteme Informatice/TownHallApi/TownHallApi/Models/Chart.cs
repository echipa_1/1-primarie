﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TownHallApi.Models
{
    public class Chart
    {
        public int Id { get; set; }
        public string Department { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Occupation { get; set; }
        public string Title { get; set; }
    }
}