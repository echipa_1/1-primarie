﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TownHallApi.Models
{
    public class DataContext : DbContext
    {
        public DbSet<News> News { get; set; }

        public DbSet<Chart> Charts { get; set; }
    }
}