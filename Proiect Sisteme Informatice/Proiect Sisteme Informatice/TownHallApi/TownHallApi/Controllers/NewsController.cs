﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TownHallApi.Models;

namespace TownHallApi.Controllers
{
    public class NewsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/News
        public IQueryable<News> GetNews()
        {
            return db.News;
        }

        // GET: api/News/5
        [ResponseType(typeof(News))]
        public IHttpActionResult GetNewsModel(int id)
        {
            News newsModel = db.News.Find(id);
            if (newsModel == null)
            {
                return NotFound();
            }

            return Ok(newsModel);
        }

        // PUT: api/News/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNewsModel(int id, News newsModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != newsModel.NewsId)
            {
                return BadRequest();
            }

            db.Entry(newsModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/News
        [ResponseType(typeof(News))]
        public IHttpActionResult PostNewsModel(News newsModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.News.Add(newsModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = newsModel.NewsId }, newsModel);
        }

        // DELETE: api/News/5
        [ResponseType(typeof(News))]
        public IHttpActionResult DeleteNewsModel(int id)
        {
            News newsModel = db.News.Find(id);
            if (newsModel == null)
            {
                return NotFound();
            }

            db.News.Remove(newsModel);
            db.SaveChanges();

            return Ok(newsModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NewsModelExists(int id)
        {
            return db.News.Count(e => e.NewsId == id) > 0;
        }
    }
}