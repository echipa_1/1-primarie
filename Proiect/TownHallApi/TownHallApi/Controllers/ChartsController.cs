﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TownHallApi.Models;

namespace TownHallApi.Controllers
{
    public class ChartsController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Charts
        public IQueryable<Chart> GetChartEntryModels()
        {
            return db.Charts;
        }

        // GET: api/Charts/5
        [ResponseType(typeof(Chart))]
        public IHttpActionResult GetChart(int id)
        {
            Chart chart = db.Charts.Find(id);
            if (chart == null)
            {
                return NotFound();
            }

            return Ok(chart);
        }

        // GET: api/Charts/Taxe
        public IQueryable<Chart> GetCharts(string title)
        {
            IQueryable<Chart> charts = db.Charts.Where(c => c.Title==title);

            return charts;
        }

        // PUT: api/Charts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutChart(int id, Chart chart)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != chart.Id)
            {
                return BadRequest();
            }

            db.Entry(chart).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChartExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Charts
        [ResponseType(typeof(Chart))]
        public IHttpActionResult PostChart(Chart chart)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Charts.Add(chart);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = chart.Id }, chart);
        }

        // DELETE: api/Charts/5
        [ResponseType(typeof(Chart))]
        public IHttpActionResult DeleteChart(int id)
        {
            Chart chart = db.Charts.Find(id);
            if (chart == null)
            {
                return NotFound();
            }

            db.Charts.Remove(chart);
            db.SaveChanges();

            return Ok(chart);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ChartExists(int id)
        {
            return db.Charts.Count(e => e.Id == id) > 0;
        }
    }
}